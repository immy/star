<!DOCTYPE html>
<?php
$page = isset($_GET['page']) ? $_GET['page'] : 1;

$records_per_page = 5;

$from_record_num = ($records_per_page * $page) - $records_per_page;

include_once 'includes/config.php';
include_once 'includes/crud_formation.php';

$database = new Config();
$db = $database->getConnection();

$product = new  Data($db);

$stmt = $product->readAll($page, $from_record_num, $records_per_page);
$num = $stmt->rowCount();

?>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Candidature - Star ENGINEERING</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Muli:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

 
</head>

<body>

  <!-- ======= Top Bar ======= -->
  <section id="topbar" class="d-none d-lg-block">
    <div class="container d-flex">
      <div class="contact-info mr-auto">
        <i class="icofont-envelope"></i><a href="mailto:contact@example.com">star@star-consult.com</a>
        <i class="icofont-phone"></i> (+216) 71 95 15 08
      </div>
   
    </div>
  </section>

  <!-- ======= Header ======= -->
  <header id="header">
    <div class="container d-flex">

      <div class="logo mr-auto">
        <h1 class="text-light"><a href="accueil.html">Star ENGINEERING</a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
      </div>

    <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li><a href="accueil.html">Accueil</a></li>
		  <li class="drop-down"><a href="">Société</a>
            <ul>
              <li><a href="#dd1">Qui sommes-nous?</a></li>
              
			   <li><a href="equipe.html">Equipe</a></li>
              <li><a href="expertise.html">Expertises</a></li>
              <li><a href="methode.html">Notre méthode</a></li>
              <li><a href="partenaires.html">Partenaires</a></li>
            </ul>
          </li>
    
		  	  <li class="drop-down"><a href="">A propos</a>
            <ul>
              <li><a href="apropos.html">A propos</a></li>
              <li><a href="clients.html">Clients</a></li>
              
           
            </ul>
          </li>
          <li><a href="services.html">Services</a></li>
     
          <li><a href="solutions.html"></a></li>
		  <li class="drop-down"><a href="">Solutions</a>
            <ul>
              <li><a href="solution-sageX3.html">solution-sageX3</a></li>
              <li><a href="solution-sageX3-people.html">solution-sageX3-people</a></li>
              <li><a href="solution-sageXRT.html">solution-sageXRT</a></li>
           
            </ul>
          </li>
         
		   <li class="drop-down"><a href="">Carrière</a>
            <ul>
              <li><a href="offreEmploi.html">Offres d'emploi</a></li>
              <li><a href="candidature.html">Candidature</a></li>
            
           
            </ul>
          </li>
          <li class="drop-down"><a href="">Actualité</a>
            <ul>
              <li><a href="actualite.html">Actualité</a></li>
      
            
           
            </ul>
          </li>
          <li><a href="connexion.html">Connexion</a></li>
          <li><a href="contact.html">Contact</a></li>

        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->

  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>Formation</h2>
          <ol>
            <li><a href="accueil.html">Accueil</a></li>
            <li>Formation</li>
          </ol>
        </div>

      </div>
    </section><!-- End Breadcrumbs -->

   <!-- ======= Blog Section ======= -->
    <section id="blog" class="blog">
      <div class="container">


<?php
while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
extract($row);

 ?>
	
	            <p><ul class="size"><h3><font color ='red'><?php echo "{$titre_formation}" ?></h3></font> </ul ></p>
				 <p><ul class="size"><h4><?php echo "{$nbr_place}" ?> places disponibles </ul ></p>
				<p><ul class="size"> </ul ></p>
				<p><ul class="size"><h4>De: <?php echo "{$date_debut}" ?> à <?php echo "{$date_fin}" ?></ul ></p>
				 
                <p><ul class="size"><h4>Durée: <?php echo "{$duree}" ?> </ul ></p>
				<p><ul class="size"><?php echo "{$description_formation}" ?> </ul ></p>				
			   
				<p>Ajouté le:<h3 class="left"><h4> <?php echo "{$date_ajout}" ?></h4></h3></p>
				
		  
		  
		
		
<?php

}
?>


    

      </div>
    </section><!-- End Blog Section -->
  
  </main><!-- End #main -->

  
    <!-- ======= Footer ======= -->
  <footer id="footer">

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-contact">
           <h3>Star ENGINEERING</h3>
            <p>
              Avenue Kheireddine Pacha – Immeuble « Pacha Center » – Bloc B – Bureau 5 – 2ème Etage – <br>
              1073 Tunis<br>
              TUNISIE <br><br>
              <strong>Phone:</strong> (+216) 71 95 15 08<br>
			  <strong>Fax:</strong> (+216) 71 95 15 37<br>
              <strong>Email:</strong> star@star-consult.com<br>
			  
            </p>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Liens utiles</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="accueil.html">Accueil</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="apropos.html">A propos</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="services.html">Services</a></li>
             
             
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Nos Services</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Déploiement et intégration de solutions sage</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Conseil</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Développement sage x3</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Formations sage x3</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Support</a></li>
            </ul>
          </div>
		   <div class="col-lg-4 col-md-6 footer-newsletter">
            <h4>A propos </h4>
            <p>Star ENGINEERING est un intégrateur des solutions sage x3.</p>
          
          </div>


        </div>
      </div>
    </div>

    <div class="container d-md-flex py-4">

      <div class="mr-md-auto text-center text-md-left">
        <div class="copyright">
          &copy; Copyright <strong><span>Star engineering</span></strong>. All Rights Reserved
        </div>
        <div class="credits">
          <!-- All the links in the footer should remain intact. -->
          <!-- You can delete the links only if you purchased the pro version. -->
          <!-- Licensing information: https://bootstrapmade.com/license/ -->
          <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/flattern-multipurpose-bootstrap-template/ -->
        
        </div>
      </div>
  
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/jquery-sticky/jquery.sticky.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>