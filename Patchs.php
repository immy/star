<!DOCTYPE html>
<?php
include_once 'includes/config.php';

$id = isset($_GET['id']) ? $_GET['id']: die('type produit');

$database = new Config();
$db = $database->getConnection();

include_once 'includes/crud_patch.php';
$product = new Data($db);

$a=$product-> type_produit;
$a= $id;

$query = "SELECT * FROM  " . $product->table_name . " WHERE type_produit =" .$a. " ";

        $stmt = $product->conn->prepare($query);
      
        $stmt->execute();
     

?>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Candidature - Star ENGINEERING</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Muli:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">


</head>

<body>

  <!-- ======= Top Bar ======= -->
  <section id="topbar" class="d-none d-lg-block">
    <div class="container d-flex">
      <div class="contact-info mr-auto">
        <i class="icofont-envelope"></i><a href="mailto:contact@example.com">star@star-consult.com</a>
        <i class="icofont-phone"></i> (+216) 71 95 15 08
      </div>
     
    </div>
  </section>

  <!-- ======= Header ======= -->
  <header id="header">
    <div class="container d-flex">

      <div class="logo mr-auto">
        <h1 class="text-light"><a href="accueil.html">Star ENGINEERING</a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
      </div>

    <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li><a href="accueil.html">Accueil</a></li>
		  <li class="drop-down"><a href="">Société</a>
            <ul>
              <li><a href="#dd1">Qui sommes-nous?</a></li>
              
			   <li><a href="equipe.html">Equipe</a></li>
              <li><a href="expertise.html">Expertises</a></li>
              <li><a href="methode.html">Notre méthode</a></li>
              <li><a href="partenaires.html">Partenaires</a></li>
            </ul>
          </li>
    
		  	  <li class="drop-down"><a href="">A propos</a>
            <ul>
              <li><a href="apropos.html">A propos</a></li>
              <li><a href="clients.html">Clients</a></li>
              
           
            </ul>
          </li>
          <li><a href="services.html">Services</a></li>
     
          <li><a href="solutions.html"></a></li>
		  <li class="drop-down"><a href="">Solutions</a>
            <ul>
              <li><a href="solution-sageX3.html">solution-sageX3</a></li>
              <li><a href="solution-sageX3-people.html">solution-sageX3-people</a></li>
              <li><a href="solution-sageXRT.html">solution-sageXRT</a></li>
           
            </ul>
          </li>
         
		   <li class="drop-down"><a href="">Carrière</a>
            <ul>
              <li><a href="offreEmploi.html">Offres d'emploi</a></li>
              <li><a href="candidature.html">Candidature</a></li>
            
           
            </ul>
          </li>
          <li class="drop-down"><a href="">Actualité</a>
            <ul>
              <li><a href="actualite.html">Actualité</a></li>
              <li><a href="Formations.php">Formation</a></li>
            
           
            </ul>
          </li>
          <li><a href="connexion.html">Connexion</a></li>
          <li><a href="contact.html">Contact</a></li>

        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->

  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>Patchs</h2>
          <ol>
            <li><a href="accueil.html">Accueil</a></li>
            <li>Patchs</li>
          </ol>
        </div>

      </div>
    </section><!-- End Breadcrumbs -->
<li><a href="verification.php?action=disconnect"><i class="halflings-icon off"></i> Déconnexion</a></li>
<li><a href="hotline.html"><i class="halflings-icon off"></i> Accès hotline </a></li>
  <div class="col-lg-12">
		<div class="panel panel-default">
		
	<table class="table table-bordered table-hover table-striped">
	<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="dataTable_wrapper">
					<table class="table table-striped table-bordered table-hover" id="dataTables-patchs">
	<thead>
	 <tr>
       
          <th>Nom du patch</th>
		  <th>Version patch</th>
          <th>Type du produit</th>
          <th>Description patch</th>
          <th>Lien de téléchargement</th>
          <th>Date d'ajout</th>
		  
        </tr>
	</thead>
	<tbody>
  <?php
while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
extract($row);
?>
                  <tr>
               
	<?php echo "<td>{$nom_patch}</td>" ?>
	<?php echo "<td>{$version_patch}</td>" ?>
	<?php echo "<td>{$type_produit}</td>" ?>
	<?php echo "<td>{$description_patch}</td>" ?>
	<?php echo "<td>{$lien_telechargement}</td>" ?>
	 <?php echo "<td>{$date_ajout}</td>" ?>
	
    </tr>
				  
             <?php
}
?>

	</tbody>
	</table>
	  </div>

	  </div>
			<script src="jquery.min.js"></script>
	<script src="bootstrap.min.js"></script>
	<script src="jquery.dataTables.min.js"></script>
    <script src="dataTables.bootstrap.min.js"></script>
	<script>
		$(document).ready(function() {
                $('#dataTables-patchs').DataTable({
					responsive: true
                });
		});
	</script>	

  </main><!-- End #main -->

  
    <!-- ======= Footer ======= -->
  <footer id="footer">

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-contact">
           <h3>Star ENGINEERING</h3>
            <p>
              Avenue Kheireddine Pacha – Immeuble « Pacha Center » – Bloc B – Bureau 5 – 2ème Etage – <br>
              1073 Tunis<br>
              TUNISIE <br><br>
              <strong>Phone:</strong> (+216) 71 95 15 08<br>
			  <strong>Fax:</strong> (+216) 71 95 15 37<br>
              <strong>Email:</strong> star@star-consult.com<br>
			  
            </p>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Liens utiles</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="accueil.html">Accueil</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="apropos.html">A propos</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="services.html">Services</a></li>
             
             
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Nos Services</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Déploiement et intégration de solutions sage</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Conseil</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Développement sage x3</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Formations sage x3</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Support</a></li>
            </ul>
          </div>
		   <div class="col-lg-4 col-md-6 footer-newsletter">
            <h4>A propos </h4>
            <p>Star ENGINEERING est un intégrateur des solutions sage x3.</p>
          
          </div>


        </div>
      </div>
    </div>

    <div class="container d-md-flex py-4">

      <div class="mr-md-auto text-center text-md-left">
        <div class="copyright">
          &copy; Copyright <strong><span>Star engineering</span></strong>. All Rights Reserved
        </div>
        <div class="credits">
          <!-- All the links in the footer should remain intact. -->
          <!-- You can delete the links only if you purchased the pro version. -->
          <!-- Licensing information: https://bootstrapmade.com/license/ -->
          <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/flattern-multipurpose-bootstrap-template/ -->
        
        </div>
      </div>
  
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/jquery-sticky/jquery.sticky.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>