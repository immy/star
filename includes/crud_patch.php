<?php

class Data
{

    // database connection and table name
    public $conn;
    public $table_name = "patch";
    public $table_name2 = "admin";
    // object properties
    public $reference_patch;
    public $nom_patch;
	public $version_patch;
    public $type_produit;
    public $description_patch;
    public $lien_telechargement;
	public $date_ajout;
   
    public function __construct($db)
    {
        $this->conn = $db;
    }

	function check_admin($username,$password)
	{
		$query="SELECT username,password FROM  " . $this->table_name2 ." where username='$username' AND password='$password' ; "; 
		$stmt = $this->conn->prepare($query);
        $stmt->execute();

        return $stmt;
	
		
		
	}
    // create product
    function create()
    {

        //write query
        $query = "INSERT INTO " . $this->table_name . " values('',?,?,?,?,?,?)";

        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(1, $this->nom_patch);
		$stmt->bindParam(2, $this->version_patch);
        $stmt->bindParam(3, $this->type_produit);
        $stmt->bindParam(4, $this->description_patch);
		$stmt->bindParam(5, $this->lien_telechargement);
		$stmt->bindParam(6, $this->date_ajout);

        if ($stmt->execute()) {
            return true;
        } else {
            return false;
        }

    }

    // read clients
    function readAll($page, $from_record_num, $records_per_page)
    {

        $query = "SELECT * FROM  " . $this->table_name . " ORDER BY reference_patch DESC LIMIT {$from_record_num}, {$records_per_page}";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        return $stmt;
    }

    // used for paging clients
    public function countAll()
    {

        $query = "SELECT reference_patch FROM " . $this->table_name . "";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        $num = $stmt->rowCount();

        return $num;
    }

    // used when filling up the update client form
    function readOne()
    {

        $query = "SELECT * FROM  " . $this->table_name . " WHERE reference_patch = ? LIMIT 0,1";

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->reference_patch);
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        $this->nom_patch = $row['nom_patch'];
		$this->version_patch = $row['version_patch'];
        $this->type_produit = $row['type_produit'];
        $this->description_patch= $row['description_patch'];
        $this->lien_telechargement = $row['lien_telechargement'];
		$this->date_ajout = $row['date_ajout'];
		
    }

    // update the client
    function update()
    {

      
  

        $query = "UPDATE
					" . $this->table_name . "
				SET
					nom_patch = :np,
					version_patch = :vp,
					type_produit = :tp,
					description_patch = :dp,
					lien_telechargement = :lt,
					date_ajout = :da
					
					 
				WHERE
					reference_patch = :rp";

        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(':np', $this->nom_patch);
		$stmt->bindParam(':vp', $this->version_patch);
        $stmt->bindParam(':tp', $this->type_produit);
        $stmt->bindParam(':dp', $this->description_patch);
        $stmt->bindParam(':lt', $this->lien_telechargement);
        $stmt->bindParam(':da', $this->date_ajout);  
		
		 $stmt->bindParam(':rp', $this->reference_patch);
        // execute the query
        if ($stmt->execute()) {
            return true;
        } else {
            return false;
        }
    }

    // delete the product
    function delete()
    {

        $query = "DELETE FROM " . $this->table_name . " WHERE reference_patch = ?";

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->id);

        if ($result = $stmt->execute()) {
            return true;
        } else {
            return false;
        }
    }
}

?>
