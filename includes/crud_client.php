<?php

class Data
{

    // database connection and table name
    public $conn;
    public $table_name = "client";
    public $table_name2 = "admin";
    // object properties
    public $reference_client;
    public $nom_client;
    public $email;
    public $login;
    public $mdp;
    public $status;
	public $produit_maintenu;

    public function __construct($db)
    {
        $this->conn = $db;
    }

	function check_admin($username,$password)
	{
		$query="SELECT username,password FROM  " . $this->table_name2 ." where username='$username' AND password='$password' ; "; 
		$stmt = $this->conn->prepare($query);
        $stmt->execute();

        return $stmt;
	
		
		
	}
    // create product
    function create()
    {

        //write query
        $query = "INSERT INTO " . $this->table_name . " values('',?,?,?,?,?,?)";

        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(1, $this->nom_client);
        $stmt->bindParam(2, $this->email);
        $stmt->bindParam(3, $this->login);
        $stmt->bindParam(4, $this->mdp);
		$stmt->bindParam(5, $this->status);
		$stmt->bindParam(6, $this->produit_maintenu);
        if ($stmt->execute()) {
            return true;
        } else {
            return false;
        }

    }

    // read clients
    function readAll($page, $from_record_num, $records_per_page)
    {

        $query = "SELECT * FROM  " . $this->table_name . " ORDER BY reference_client DESC LIMIT {$from_record_num}, {$records_per_page}";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        return $stmt;
    }

    // used for paging clients
    public function countAll()
    {

        $query = "SELECT reference_client FROM " . $this->table_name . "";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        $num = $stmt->rowCount();

        return $num;
    }

    // used when filling up the update client form
    function readOne()
    {

        $query = "SELECT * FROM  " . $this->table_name . " WHERE reference_client = ? LIMIT 0,1";

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->reference_client);
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        $this->nom_client = $row['nom_client'];
        $this->email = $row['email'];
        $this->login = $row['login'];
        $this->mdp = $row['mdp'];
		$this->status= $row['status'];
		$this->produit_maintenu= $row['produit_maintenu'];
		
    }
  
    // update the client
    function update()
    {


        $query = "UPDATE
					" . $this->table_name . "
				SET
					nom_client = :nc,
					email = :em,
					login = :lg,
					mdp = :mdp,
					status = :status,
					produit_maintenu = :produit_maintenu
					
					 
				WHERE
					reference_client = :rc";

        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(':nc', $this->nom_client);
        $stmt->bindParam(':em', $this->email);
        $stmt->bindParam(':lg', $this->login);
        $stmt->bindParam(':mdp', $this->mdp);
		$stmt->bindParam(':status', $this->status);
		$stmt->bindParam(':produit_maintenu', $this->produit_maintenu);
  
		 $stmt->bindParam(':rc', $this->reference_client);
        // execute the query
        if ($stmt->execute()) {
            return true;
        } else {
            return false;
        }
    }

    // delete the product
    function delete()
    {

        $query = "DELETE FROM " . $this->table_name . " WHERE reference_client = ?";

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->id);

        if ($result = $stmt->execute()) {
            return true;
        } else {
            return false;
        }
    }
}

?>
