<?php

class Data
{

    // database connection and table name
    public $conn;
    public $table_name = "formation";
    public $table_name2 = "admin";
    // object properties
    public $reference_formation;
    public $titre_formation;
    public $nbr_place;
    public $date_ajout;
    public $date_debut;
    public $date_fin;
    public $duree;
	public $description_formation;
	
    public function __construct($db)
    {
        $this->conn = $db;
    }

	function check_admin($username,$password)
	{
		$query="SELECT username,password FROM  " . $this->table_name2 ." where username='$username' AND password='$password' ; "; 
		$stmt = $this->conn->prepare($query);
        $stmt->execute();

        return $stmt;
	
		
		
	}
	
    // create product
    function create()
    {

        //write query
        $query = "INSERT INTO " . $this->table_name . " values('',?,?,?,?,?,?,?)";

        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(1, $this->titre_formation);
        $stmt->bindParam(2, $this->nbr_place);
        $stmt->bindParam(3, $this->date_ajout);
        $stmt->bindParam(4, $this->date_debut);
		$stmt->bindParam(5, $this->date_fin);
		$stmt->bindParam(6, $this->duree);
		$stmt->bindParam(7, $this->description_formation);
        if ($stmt->execute()) {
            return true;
        } else {
            return false;
        }

    }

    // read clients
    function readAll($page, $from_record_num, $records_per_page)
    {

        $query = "SELECT * FROM  " . $this->table_name . " ORDER BY reference_formation DESC LIMIT {$from_record_num}, {$records_per_page}";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        return $stmt;
    }

    // used for paging clients
    public function countAll()
    {

        $query = "SELECT reference_formation FROM " . $this->table_name . "";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        $num = $stmt->rowCount();

        return $num;
    }

    // used when filling up the update client form
    function readOne()
    {

        $query = "SELECT * FROM  " . $this->table_name . " WHERE reference_formation = ? LIMIT 0,1";

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->reference_formation);
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        $this->titre_formation = $row['titre_formation'];
        $this->nbr_place = $row['nbr_place'];
        $this->date_ajout = $row['date_ajout'];
        $this->date_debut = $row['date_debut'];
		$this->date_fin = $row['date_fin'];
        $this->duree = $row['duree'];
        $this->description_formation = $row['description_formation'];
		
	
 
		
    }

    // update the client
    function update()
    {


        $query = "UPDATE
					" . $this->table_name . "
				SET
		
		
					titre_formation = :tf,
					nbr_place = :np,
					date_ajout = :da,
					date_debut= :db,
					date_fin = :df,
					duree= :duree,
					description_formation= :desf
					
					 
				WHERE
					reference_formation = :rf";

        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(':tf', $this->titre_formation);
        $stmt->bindParam(':np', $this->nbr_place);
        $stmt->bindParam(':da', $this->date_ajout);
        $stmt->bindParam(':db', $this->date_debut);
        $stmt->bindParam(':df', $this->date_fin);
        $stmt->bindParam(':duree', $this->duree);
		$stmt->bindParam(':desf', $this->description_formation);
		 $stmt->bindParam(':rf', $this->reference_formation);
        // execute the query
        if ($stmt->execute()) {
            return true;
        } else {
            return false;
        }
    }

    // delete the product
    function delete()
    {

        $query = "DELETE FROM " . $this->table_name . " WHERE reference_formation = ?";

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->id);

        if ($result = $stmt->execute()) {
            return true;
        } else {
            return false;
        }
    }
}

?>
