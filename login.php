<!DOCTYPE html>

<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Candidature - Star ENGINEERING</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Muli:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">


</head>

<body>

  <!-- ======= Top Bar ======= -->
  <section id="topbar" class="d-none d-lg-block">
    <div class="container d-flex">
      <div class="contact-info mr-auto">
        <i class="icofont-envelope"></i><a href="mailto:contact@example.com">star@star-consult.com</a>
        <i class="icofont-phone"></i> (+216) 71 95 15 08
      </div>
   
    </div>
  </section>

  <!-- ======= Header ======= -->
  <header id="header">
    <div class="container d-flex">

      <div class="logo mr-auto">
        <h1 class="text-light"><a href="accueil.html">Star ENGINEERING</a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
      </div>

    <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li class="active"><a href="accueil.html">Accueil</a></li>
		  <li class="drop-down"><a href="">Société</a>
            <ul>
              <li><a href="#dd1">Qui sommes-nous?</a></li>
              
			   <li><a href="equipe.html">Equipe</a></li>
              <li><a href="expertise.html">Expertises</a></li>
              <li><a href="methode.html">Notre méthode</a></li>
              <li><a href="partenaires.html">Partenaires</a></li>
            </ul>
          </li>
    
		  	  <li class="drop-down"><a href="">A propos</a>
            <ul>
              <li><a href="apropos.html">A propos</a></li>
              <li><a href="clients.html">Clients</a></li>
              
           
            </ul>
          </li>
          <li><a href="services.html">Services</a></li>
     
          <li><a href="solutions.html"></a></li>
		  <li class="drop-down"><a href="">Solutions</a>
            <ul>
              <li><a href="solution-sageX3.html">solution-sageX3</a></li>
              <li><a href="solution-sageX3-people.html">solution-sageX3-people</a></li>
              <li><a href="solution-sageXRT.html">solution-sageXRT</a></li>
           
            </ul>
          </li>
         
		   <li class="drop-down"><a href="">Carrière</a>
            <ul>
              <li><a href="offreEmploi.html">Offres d'emploi</a></li>
              <li><a href="candidature.html">Candidature</a></li>
            
           
            </ul>
          </li>
          <li class="drop-down"><a href="">Actualité</a>
            <ul>
              <li><a href="actualite.html">Actualité</a></li>
              <li><a href="Formations.php">Formation</a></li>
            
           
            </ul>
          </li>
          <li class="active"><a href="connexion.html">Connexion</a></li>
          <li><a href="contact.html">Contact</a></li>

        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->

  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>Connexion</h2>
          <ol>
            <li><a href="accueil.html">Accueil</a></li>
            <li>Connexion</li>
          </ol>
        </div>

      </div>
    </section><!-- End Breadcrumbs -->

   <!-- ======= Blog Section ======= -->
    <section id="blog" class="blog">
      <div class="container">




        <div class="row">

          <div class="col-lg-8 entries">

            <div class="blog-comments" data-aos="fade-up">

              <div class="reply-form">
               <h4>Bienvenu à notre espace client !</h4>
				<p></p>
               

               <form action="verification.php" method="POST">
				    <div class="row">
					<div class="col form-group">
					<input type="text" placeholder="Entrer le nom d'utilisateur" name="username" required>
					</div>
                    </div>
			
					<div class="row">
                    <div class="col form-group">
                    <input type="password" placeholder="Entrer le mot de passe" name="password" required>
				    </div>
                    </div>
					<p>Vueillez choisir le nom du produit:</p>
					<div class="form-group">
                    
                    <div class="col-lg-10">
                      <label class="checkbox-inline">
                                              <input type="checkbox" name="produit_maintenu" id="produit_maintenu1" value="x3"> x3
                                          </label>
                      <label class="checkbox-inline">
                                              <input type="checkbox" name="produit_maintenu" id="produit_maintenu2" value="xrt"> xrt
                                          </label>
                      <label class="checkbox-inline">
                                              <input type="checkbox" name="produit_maintenu" id="produit_maintenu3" value="people"> people
                                          </label>

                    </div>
                  </div>

                
               <button type="submit" id='submit'  class="btn btn-primary">Login</button>
				<?php
                if(isset($_GET['erreur'])){
                    $err = $_GET['erreur'];
                    if($err==1 || $err==2)
                        echo "<p style='color:red'>Utilisateur ou mot de passe incorrect</p>";
                }
                ?>
            </form>
              </div>

            </div>

      </div>

        </div>

      </div>
    </section><!-- End Blog Section -->
  <!-- ======= Blog Section ======= -->
    <section id="blog" class="blog">
      <div class="container">

        <div class="row">

          <div class="col-lg-8 entries">

            <div class="blog-comments" data-aos="fade-up">

              <div class="reply-form">
                <form action="">
            
				
                </form>

              </div>

            </div>

      </div>

        </div>

      </div>
    </section><!-- End Blog Section -->
  </main><!-- End #main -->

  
    <!-- ======= Footer ======= -->
  <footer id="footer">

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-contact">
           <h3>Star ENGINEERING</h3>
            <p>
              Avenue Kheireddine Pacha – Immeuble « Pacha Center » – Bloc B – Bureau 5 – 2ème Etage – <br>
              1073 Tunis<br>
              TUNISIE <br><br>
              <strong>Phone:</strong> (+216) 71 95 15 08<br>
			  <strong>Fax:</strong> (+216) 71 95 15 37<br>
              <strong>Email:</strong> star@star-consult.com<br>
			  
            </p>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Liens utiles</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="accueil.html">Accueil</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="apropos.html">A propos</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="services.html">Services</a></li>
             
             
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Nos Services</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Déploiement et intégration de solutions sage</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Conseil</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Développement sage x3</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Formations sage x3</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Support</a></li>
            </ul>
          </div>
		   <div class="col-lg-4 col-md-6 footer-newsletter">
            <h4>A propos </h4>
            <p>Star ENGINEERING est un intégrateur des solutions sage x3.</p>
          
          </div>


        </div>
      </div>
    </div>

    <div class="container d-md-flex py-4">

      <div class="mr-md-auto text-center text-md-left">
        <div class="copyright">
          &copy; Copyright <strong><span>Star engineering</span></strong>. All Rights Reserved
        </div>
     
      </div>
  
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/jquery-sticky/jquery.sticky.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>